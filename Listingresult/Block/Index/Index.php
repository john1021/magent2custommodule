<?php
/**
 * Copyright © 2015 Employee . All rights reserved.
 */
namespace Employee\Listingresult\Block\Index;
use Employee\Listingresult\Block\BaseBlock;

use Magento\Framework\View\Element\Template\Context;
use Employee\Listingresult\Model\Contact;
use Magento\Framework\View\Element\Template;

class Index extends BaseBlock
{
	public function __construct(Context $context, Contact $model)
	{
                $this->model = $model;
		parent::__construct($context);
                
	}
        
	public function sayHello()
	{
		return __('Hello World');
	}
        public function getHelloCollection()
        {
            $helloCollection = $this->model->getCollection();
            return $helloCollection;
        }
	
}
